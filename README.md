# README #

This README would normally document whatever steps are necessary to get your application up and running.

Installation steps: 

$ sudo apt-get install python-pip 

$ sudo apt-get install python-dev 

$ sudo pip install scrapy 

Official tutorial
http://doc.scrapy.org/en/latest/topics/selectors.html#topics-selectors

Usage:

$ git clone https://gavingo15@bitbucket.org/gavingo15/scrapy_zillow.git

$ cd scrapy_zillow

$ gcc crawler/compute_url.c -o crawler/compute_url

$ ./crawler/compute_url 45.651893 45.431512 -122.470801 -122.837341 0.026391 0.05579 > testcase

If result/ folder exists, remove it: $ rm -r result/

$ mkdir result && cd result

$ ../crawler/start all.json < ../testcase

$ cd ..

$ ./crawler/combine_result.sh result/ result.json

result/result.json is the result of crawling house information from Zillow on Portland area.

$ cat result/result.json

There are 2456 house infomation, but there are 2,974 in Zillow Portland area.
The reason of difference number between our crawling data and Zillow data is still under investigation.
This bug will be fixed afterward.

Example result folder "example_result/result/" shows the example result when executing the instructions.
Note: Please do not use combine_result.sh with this folder as argument to combine the final result, it will occurs some error.