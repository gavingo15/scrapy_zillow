import scrapy
import sys

class ZillowSpider(scrapy.Spider):
    name = 'zillow'
    start_urls = ['http://www.zillow.com/homes/for_sale/Portland-OR/13373_rid/featured_sort/45.440773,-122.805565,45.414383,-122.861359_rect/14_zm/1_fr/']

    def parse(self, response):
        #for href in response.css('.question-summary h3 a::attr(href)'):
        #for href in response.css('.statusType a::attr(href)'):
        #for href in response.css('a[href*=image]::attr(href)'):
        for href in response.css('a[class*="routable mask hdp-link"]::attr(href)'):
            full_url = response.urljoin(href.extract())
            yield scrapy.Request(full_url, callback=self.parse_question)
	
        current_number_list = response.xpath('//li[@class="zsg-pagination_active"]/a/text()').extract() #get current page number list
	if current_number_list:
            for href in response.css('a[onclick*="SearchMain.changePage('+str(int(current_number_list[0])+1)+');return false;"]::attr(href)'):
                full_url = response.urljoin(href.extract())
                yield scrapy.Request(full_url, callback=self.parse)

    def parse_question(self, response):
        yield {
            #'title': response.css('h1 a::text').extract()[0],
            #'votes': response.css('.question .vote-count-post::text').extract()[0],
            #'body': response.css('.question .post-text').extract()[0],
            #'tags': response.css('.question .post-tag::text').extract(),
            'address': response.xpath('//title/text()').re(r'(.*)  | Zillow')[0],
            'name': response.xpath('//header[@class="zsg-content-header addr"]/h1/text()').extract()[0],
            'zip code': response.xpath('//span[@class="zsg-h2 addr_city"]/text()').extract()[0],
            'beds': response.xpath('//span[@class="addr_bbs"]/text()')[0].re(r'(.*) bed')[0],
            'baths': response.xpath('//span[@class="addr_bbs"]/text()')[1].re(r'(.*) bath')[0],
            'sqft': response.xpath('//span[@class="addr_bbs"]/text()')[2].re(r'(.*) sqft')[0],
            'purchase price': response.xpath('//div[@class="main-row  home-summary-row"]/span/text()').extract()[0],
            'zestimate price': response.xpath('//div[@class="  home-summary-row"]/span[@class=""]/text()').extract()[0],
            #'rent price': response.xpath('//span[@class="hlc-output-fixed30"]').extract(),
	    # rent price contains bug, it's value is none
            'description': response.xpath('//div[@class="notranslate"]/text()').extract()[0],
            'latitude': response.css('span[id*="hdp-map-coordinates"]::attr(data-latitude)').extract(),
            'longitude': response.css('span[id*="hdp-map-coordinates"]::attr(data-longitude)').extract(),
            'link': response.url,
        }
