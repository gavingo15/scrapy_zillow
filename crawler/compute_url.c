#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    if (argc != 7) {
      printf("Usage: ./compute_url <top> <bottom> <right> <left> <vertical_interval> <horizontal_interval> >> testcase\n");
      return 0;
    }
/*
    float top = strtol(argv[1]);
    float bottom = strtol(argv[2]);
    float left = strtol(argv[3]);
    float right = strtol(argv[4]);
*/
    float top = atof(argv[1]), tmp_top = top;
    float bottom = atof(argv[2]), tmp_bottom = bottom;
    float right = atof(argv[3]), tmp_right = right;
    float left = atof(argv[4]), tmp_left = left;
    float vertical_interval = atof(argv[5]);
    float horizontal_interval = atof(argv[6]);
//    printf("top=%f , bottom=%f , left=%f , right=%f\n", 
//            top, bottom, left, right);

    for( ; tmp_top > tmp_bottom ; tmp_top = tmp_top - vertical_interval ) {
        for( ; tmp_right > tmp_left ; tmp_right = tmp_right - horizontal_interval )
          printf("http://www.zillow.com/homes/for_sale/Portland-OR/13373_rid/featured_sort/%f,%f,%f,%f_rect/14_zm/1_fr/\n",tmp_top,tmp_right,(tmp_top-vertical_interval), (tmp_right-horizontal_interval));
        tmp_right = right;
    }
    printf("#ByeScrapy");
    return 0;
}
