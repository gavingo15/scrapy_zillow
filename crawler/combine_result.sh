#!/bin/sh
####### Usage : ./combine_result.sh <result directory> <destination file>
####### This scripy will collect contents of all file located in <result directory>,
####### and store into <destination file>
####### Example ./start.sh result/ result.json

if [ "$2" = "" ]; then
  echo "./combine_result.sh <result directory> <destination file>"
  echo "This scripy will collect contents of all file located in <result directory>,"
  echo "and store into <destination file>"
  echo "Example ./crawler/combine_result.sh result/ result.json"
  echo "The execution result file is in result/result.json"
  exit 0
fi

#grep -v "\[$" test/e.txt
#sed -i '/\[$/d' test/e.txt

check="$(echo "$1" | grep "/")"
#echo "check="$check
if [ "$check" = "" ]; then
  if [ -e "$1/$2" ]; then # if file exists, remove it
    rm $1"/"$2
  fi
  du -sh $1"/*" | grep "4.0K" > tmp # list all file size into tmp
  while read line           
  do    
    file_tmp="$(echo $line | sed 's/4.0K.//g')"
    rm $file_tmp # delete files which do not contain information
  done <tmp
  rm tmp
  # read tmp file and check if any file size is too small, remove these files
  cat "$1/"* > $1"/"$2
  sed -i '/\[$/d' $1"/"$2
  sed -i -- 's/\]\[/,\n/g' $1"/"$2 # replace '][' to ',\n' to maintain the correctness of json format
  #all_result=$(cat "$1/"*)
  #print $all_result
  #for line in $all_result 
  #do
  #  if [ "$line" != "[" ]; then
  #    echo "$line" >> $1"/"$2
  #  fi;
    #cat $1"/"$2
  #done
else
#  ls $1*
  if [ -e "$1$2" ]; then
    rm $1$2
  fi
  du -sh $1* | grep "4.0K" > tmp # list all file size into tmp
  while read line           
  do    
    file_tmp="$(echo $line | sed 's/4.0K.//g')"
    rm $file_tmp # delete files which do not contain information
  done <tmp
  rm tmp
  cat $1* > $1$2
  sed -i '/\[$/d' $1$2
  sed -i -- 's/\]\[/,\n/g' $1$2 # replace '][' to ',\n' to maintain the correctness of json format
  #all_result=$(cat $1*)
  #print $all_result
  #for line in $all_result
  #do
  #  if [ "$line" != "[" ]; then
  #    echo "$line" >> $1$2
  #  fi;
    #cat $1$2
  #done
fi;
